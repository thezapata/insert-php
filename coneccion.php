<?php

/*
 * Funcion para conectar php con mysql (o con Maria DB);
 */

function conect(){
	$host = "localhost"; //o la ip del servidor
	$user = "root";
	$password = "";
	$database = "bdusuarios_web";
	$port = 3306;
	$socket = "";
	if (!($link = mysqli_connect($host, $user, $password, $database, $port, $socket))) {
		echo "No se puede conectar a la base de datos";
		exit();
	}

	if (!mysqli_select_db($link, $database)) {
		echo "No se puede conectar a la base de datos";
		exit();
	}

	return $link;
}