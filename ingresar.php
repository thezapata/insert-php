<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Untitled</title>
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
    	<!-- Formulario que recoje los datos para agregarlos a la BD -->
        <form action="ingreso.php" id="frmAgregar" name="frmAgregar" method="post">
        	<table>
	        	<tr>
	        		<td>Usuario</td>
	        		<td><input type="text" maxlength="20" placeholder="User" name="txtUser" id="txtUser"></td>
	        	</tr>
	        	<tr>
	        		<td>Nombre</td>
	        		<td><input type="text" maxlength="20" placeholder="Nombre" name="txtNom" id="txtNom"></td>
	        	</tr>
	        	<tr>
	        		<td>Correo</td>
	        		<td><input type="email" placeholder="ejemplo@correo.com" name="txtMail" id="txtMail"></td>
	        	</tr>
	        	<tr>
	        		<td>Clave</td>
	        		<td><input type="password" name="txtPas" id="txtPas"></td>
	        	</tr>
	        	<tr>
	        		<td>foto</td>
	        		<td><input type="text" placeholder="http://link-de-la-imagen" name="txtFoto" id="txtFoto"></td>
	        	</tr>
	        	<tr>
	        		<td><input type="submit" value="Guardar" class="puntero"></td>
	        		<td><input type="reset" value="Cancelar" class="puntero"></td>
	        	</tr>
	        	<tr>
	        		<td colspan="2" class="centro">
	        			<input type="button" value="Volver" class="puntero" onclick="inicio()">
	        		</td>
	        	</tr>
        	</table>
        </form>
        <script src="js/main.js"></script>
    </body>
</html>